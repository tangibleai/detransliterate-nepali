# Detransliteration of Nepali Devangari

[Free tool at romantonepali.com](https://romantonepali.com/)

1. Upper/Lower case doesn’t matter, EXCEPT:

ta = त  Ta = ट  tha = थ Tha = ठ
da = द  Da = ड  dha = ध Dha = ढ
na = न  Na = ण  sha = श Sha = ष

2. If a letter gets wrongly combined with the next letter, use the slash (/) key to separate. Example:
pratinidhi = प्रतीनइधी
prati/nidhi = प्रतिनिधी

3. If you need to embed English into your text, put the English text inside curly braces {}. Example:
yo {software} kasto chha ? =
यो software कस्तो छ?

4. Other special characters:

ri^ = ्रि (as in प्रि)  rr = र्‍ (as in गर्‍यो) rri = ऋ rree = ॠ    yna = ञ chha = छ    ksha = क्ष  gyna = ज्ञ  * = अनुस्वर ** = चन्द्रबिन्दु   om = ॐ

5. Use of Halant हलन्त

To force a हलन्त at the end of a word, use the backslash (\) key. Example: sunnuhos\ = सुन्नुहोस्
