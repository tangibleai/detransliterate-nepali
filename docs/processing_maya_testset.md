

Save the dataset to the data dir, then use these commands to encode and decode strings from the dataset.
Encoding turns multi-byte characters into byte strings, where each character is a normal ascii character so there are control codes, etc. 
Decoding turns sequences of multiple bytes (1-4 bytes) into multibyte unicode characters.

```python
>>> nepali = "केहि पनि चाहिदैन"
>>> nepali
'केहि पनि चाहिदैन'
>>> import pandas as pd
>>> pd.read_csv('data/Maya Transliteration Dataset.csv')
                              transliterated_sentence                                 devangari_sentence
0                                               Malai                                               मलाई
...
>>> df = _
>>> df.devangari_sentence
0                                                  मलाई
>>> df.devangari_sentence.str.len()
0      4
1     10
2     18
3      5
4      9
5      4
...
22     7
23    21
24    28
25    30
26    39
27    12
28    13
Name: devangari_sentence, dtype: int64
>>> df.devangari_sentence[28]
'कस्तो प्रश्न '
>>> df.devangari_sentence[28].decode()
ValueError or Encode/Decode error: it's alread a unicdoe str and not bytes)
>>> df.devangari_sentence[28].encode()
b'\xe0\xa4\x95\xe0\xa4\xb8\xe0\xa5\x8d\xe0\xa4\xa4\xe0\xa5\x8b \xe0\xa4\xaa\xe0\xa5\x8d\xe0\xa4\xb0\xe0\xa4\xb6\xe0\xa5\x8d\xe0\xa4\xa8 '
>>> df.devangari_sentence[28].encode().decode()
'कस्तो प्रश्न '
```
