#install Google Transliteration API
def detransliterate_nepali(text):
    from google.transliteration import transliterate_text
    return transliterate_text(text, lang_code='ne')